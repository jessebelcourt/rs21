var express = require("express"),
				app = express();

app.use(express.static(__dirname + '/'));

app.get('/', function (req, res) {
	res.sendFile(__dirname + './index.html');
});

app.listen(8060, function () {
	console.log('Listening on 8060');
});